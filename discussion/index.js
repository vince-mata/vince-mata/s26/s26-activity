//Use the "require" directive to load the Node.js modules A module is a
//software component or part of a program that contains one or more
//routines. http module - lets the node.js to transfer data using the

//Hyper Text Transfer Protocol http module- is a set of individual
//files that contain code to create a "component" that helps establish
//data transfer between application HTTP is a protocol that allows the
//fetching of resources such as HTML documents

let http = require("http");

/*
Using this module's createServer() method, we can create an HTTP server
that listens to request on a specified port and gives responses back to
the client.

The http module has a createServer()method that accepts a function as an
argument and allows a creation of server.


//The server will be assigned to port 4000 via the "listen(4000)" method
  where the server will listen to any requests that are sent to it
  eventually communicating with our server


*/ 

http.createServer(function(request, response){
    //Use the writeHead method to:
    //Set a status code for the response- 200 means OK
    //Set the content-type of the response as a plain text message

    response.writeHead(200, {'Content-Type': 'text/plain'});

    //end() ends the response of the server and send the data or message
    response.end("Hello World");


}).listen(4000);

//When the server is running, console will print the message
console.log('Server running at localhost: 4000');
