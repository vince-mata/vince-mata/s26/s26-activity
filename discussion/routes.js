const http = require("http");

//creates a variable "port" to store the port number
const port = 4000;

const server = http.createServer( (req, res) => {

	if(req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello, Batch 157!")
	}

	//Accessing the "homepage" route returns a message of "This is the homepage"
	else if (req.url == '/homepage'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("This is the homepage!");
	} 

	//All the other routes will return a message of "Page not available"
	else {

		//Set a status code for the response - 404 means Not Found
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Page not available');
	}

});

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);


